# Socker

## Using the rocker/rstudio/stics container

### Quickstart

```
docker run -e PASSWORD=<whateverpasswordyouset> --rm -p 8787:8787 registry.forgemia.inra.fr/patrick.chabrier/sticsquays/rstudio_stics:9.2
```

Visit localhost:8787 in your browser and log in with _username_ **rstudio** and the _password_ **whateverpasswordyouset**. NB: Setting a password is now REQUIRED. Container will error otherwise.
